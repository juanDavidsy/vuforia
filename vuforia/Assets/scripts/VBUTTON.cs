using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBUTTON : MonoBehaviour
{
    #region variables
    public GameObject canvas;
    public VirtualButtonBehaviour vb;

    #endregion

    #region funciones
    void Start()
    {
        vb.RegisterOnButtonPressed(OnButtonPressed);
        vb.RegisterOnButtonReleased(OnButtonRealessed);
        canvas.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        canvas.SetActive(true);
    }
   
    public void OnButtonRealessed(VirtualButtonBehaviour vb)
    {
        canvas.SetActive(false);
    }
    #endregion

}
